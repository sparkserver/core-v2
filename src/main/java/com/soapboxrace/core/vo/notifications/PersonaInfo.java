/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.vo.notifications;

import com.soapboxrace.core.jpa.PersonaEntity;

public class PersonaInfo {
    private String name;
    private long id;
    private long userId;

    PersonaInfo(PersonaEntity persona) {
        this.name = persona.getName();
        this.id = persona.getPersonaId();
        this.userId = persona.getUser().getId();
    }
}
