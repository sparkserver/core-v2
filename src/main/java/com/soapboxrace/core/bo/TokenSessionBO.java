/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.bo;

import com.soapboxrace.core.auth.AuthException;
import com.soapboxrace.core.auth.AuthResultVO;
import com.soapboxrace.core.auth.BanInfoVO;
import com.soapboxrace.core.auth.verifiers.PasswordVerifier;
import com.soapboxrace.core.dao.TokenSessionDAO;
import com.soapboxrace.core.dao.UserDAO;
import com.soapboxrace.core.engine.EngineException;
import com.soapboxrace.core.engine.EngineExceptionCode;
import com.soapboxrace.core.jpa.BanEntity;
import com.soapboxrace.core.jpa.TokenSessionEntity;
import com.soapboxrace.core.jpa.UserEntity;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Stateless
public class TokenSessionBO {
    @EJB
    private TokenSessionDAO tokenDAO;

    @EJB
    private UserDAO userDAO;

    @EJB
    private ParameterBO parameterBO;

    @EJB
    private GetServerInformationBO serverInfoBO;

    @EJB
    private AuthenticationBO authenticationBO;

    public boolean verifyToken(Long userId, String securityToken) {
        TokenSessionEntity tokenSessionEntity = tokenDAO.findById(securityToken);
        if (tokenSessionEntity == null || !tokenSessionEntity.getUserEntity().getId().equals(userId)) {
            return false;
        }
        long time = new Date().getTime();
        long tokenTime = tokenSessionEntity.getExpirationDate().getTime();
        return time <= tokenTime;
    }

    public String createToken(Long userId, String clientHostName) {
        TokenSessionEntity tokenSessionEntity = new TokenSessionEntity();
        Date expirationDate = getMinutes(parameterBO.getIntParam("SESSION_LENGTH_MINUTES", 130));
        tokenSessionEntity.setExpirationDate(expirationDate);
        String randomUUID = UUID.randomUUID().toString();
        tokenSessionEntity.setSecurityToken(randomUUID);
        UserEntity userEntity = userDAO.findById(userId);
        tokenSessionEntity.setUserEntity(userEntity);
        tokenSessionEntity.setPremium(userEntity.isPremium());
        tokenSessionEntity.setClientHostIp(clientHostName);
        tokenSessionEntity.setActivePersonaId(0L);
        tokenSessionEntity.setEventSessionId(null);
        tokenDAO.insert(tokenSessionEntity);
        return randomUUID;
    }

    public void verifyPersonaOwnership(String securityToken, Long personaId) {
        TokenSessionEntity tokenSession = tokenDAO.findById(securityToken);
        if (tokenSession == null) {
            throw new EngineException(EngineExceptionCode.NoSuchSessionInSessionStore, true);
        }

        if (!tokenSession.getUserEntity().ownsPersona(personaId)) {
            throw new EngineException(EngineExceptionCode.RemotePersonaDoesNotBelongToUser, true);
        }
    }

    public void deleteByUserId(Long userId) {
        tokenDAO.deleteByUserId(userId);
    }

    private Date getMinutes(int minutes) {
        long time = new Date().getTime();
        time = time + (minutes * 60000);
        return new Date(time);
    }

    public AuthResultVO login(String email, PasswordVerifier password, HttpServletRequest httpRequest) throws AuthException {
        if (email == null || email.isEmpty()) {
            throw new AuthException("Invalid email or password");
        }
        UserEntity userEntity = userDAO.findByEmail(email);
        if (userEntity == null) {
            throw new AuthException("Invalid email or password");
        }
        if (!password.verifyHash(userEntity)) {
            throw new AuthException("Invalid email or password");
        }
        if (userEntity.isLocked()) {
            throw new AuthException("Account locked. Contact an administrator.");
        }

        userEntity.setIpAddress(httpRequest.getRemoteAddr());

        BanEntity banEntity = authenticationBO.checkUserBan(userEntity);

        if (banEntity != null) {
            BanInfoVO banInfoVO = new BanInfoVO(banEntity.getReason(), banEntity.getEndsAt());
            throw new AuthException(banInfoVO);
        }

        userEntity.setLastLogin(LocalDateTime.now());
        userDAO.update(userEntity);
        Long userId = userEntity.getId();
        deleteByUserId(userId);
        String randomUUID = createToken(userId, httpRequest.getRemoteHost());

        return new AuthResultVO(userId, randomUUID);
    }

    public Long getActivePersonaId(String securityToken) {
        TokenSessionEntity tokenSessionEntity = tokenDAO.findById(securityToken);
        return tokenSessionEntity.getActivePersonaId();
    }

    public void setActivePersonaId(String securityToken, Long personaId, Boolean isLogout) {
        TokenSessionEntity tokenSessionEntity = tokenDAO.findById(securityToken);

        if (!isLogout) {
            if (!tokenSessionEntity.getUserEntity().ownsPersona(personaId)) {
                throw new EngineException(EngineExceptionCode.RemotePersonaDoesNotBelongToUser, true);
            }
        }

        tokenSessionEntity.setActivePersonaId(personaId);
        tokenDAO.update(tokenSessionEntity);
    }

    public String getActiveRelayCryptoTicket(String securityToken) {
        TokenSessionEntity tokenSessionEntity = tokenDAO.findById(securityToken);
        return tokenSessionEntity.getRelayCryptoTicket();
    }

    public Long getActiveLobbyId(String securityToken) {
        TokenSessionEntity tokenSessionEntity = tokenDAO.findById(securityToken);
        return tokenSessionEntity.getActiveLobbyId();
    }

    public void setActiveLobbyId(String securityToken, Long lobbyId) {
        TokenSessionEntity tokenSessionEntity = tokenDAO.findById(securityToken);
        tokenSessionEntity.setActiveLobbyId(lobbyId);
        tokenDAO.update(tokenSessionEntity);
    }

    public boolean isAdmin(String securityToken) {
        return getUser(securityToken).isAdmin();
    }

    public UserEntity getUser(String securityToken) {
        return tokenDAO.findById(securityToken).getUserEntity();
    }

    public void setEventSessionId(String securityToken, Long eventSessionId) {
        TokenSessionEntity tokenSessionEntity = tokenDAO.findById(securityToken);
        tokenSessionEntity.setEventSessionId(eventSessionId);
        tokenDAO.update(tokenSessionEntity);
    }

    public Long getEventSessionId(String securityToken) {
        return tokenDAO.findById(securityToken).getEventSessionId();
    }
}