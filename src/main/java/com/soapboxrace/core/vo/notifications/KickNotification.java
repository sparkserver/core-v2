/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.vo.notifications;

import com.soapboxrace.core.jpa.PersonaEntity;

public class KickNotification {
    private PersonaInfo persona;
    private PersonaInfo kickedBy;

    public KickNotification(PersonaEntity persona, PersonaEntity kickedBy) {
        this.persona = new PersonaInfo(persona);
        if (kickedBy != null) {
            this.kickedBy = new PersonaInfo(kickedBy);
        }
    }
}
