/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.bo;

import com.soapboxrace.core.dao.HardwareInfoDAO;
import com.soapboxrace.core.jpa.HardwareInfoEntity;
import com.soapboxrace.jaxb.http.HardwareInfo;
import com.soapboxrace.jaxb.util.JAXBUtility;
import org.apache.commons.codec.digest.DigestUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class HardwareInfoBO {
    @EJB
    private HardwareInfoDAO hardwareInfoDAO;

    public HardwareInfoEntity save(HardwareInfo hardwareInfo) {
        long userId = hardwareInfo.getUserID();

        hardwareInfo.setAvailableMem(0);
        hardwareInfo.setCpuid10(0);
        hardwareInfo.setCpuid11(0);
        hardwareInfo.setCpuid12(0);
        hardwareInfo.setCpuid13(0);
        hardwareInfo.setUserID(0);

        // https://docs.microsoft.com/en-us/windows/desktop/sysinfo/operating-system-version
        // "Applications not manifested for Windows 8.1 or Windows 10 will return the Windows 8 OS version value (6.2)."
        // HOWEVER, it seems that this restriction will not apply when running inside of Sandboxie
        if (hardwareInfo.getOsMajorVersion() > 6 ||
                (hardwareInfo.getOsMajorVersion() == 6 && hardwareInfo.getOsMinorVersion() > 2)) {
            hardwareInfo.setOsMajorVersion(6);
            hardwareInfo.setOsMinorVersion(2);
            hardwareInfo.setOsBuildNumber(9200);
        }

        String hardwareInfoXml = JAXBUtility.marshal(hardwareInfo);
        String calcHardwareInfoHash = DigestUtils.sha1Hex(hardwareInfoXml);
        HardwareInfoEntity hardwareInfoEntityTmp = hardwareInfoDAO.findByUserIdAndHash(userId, calcHardwareInfoHash);
        if (hardwareInfoEntityTmp == null) {
            hardwareInfoEntityTmp = new HardwareInfoEntity();
            hardwareInfoEntityTmp.setUserId(userId);
            hardwareInfoEntityTmp.setHardwareInfo(hardwareInfoXml);
            hardwareInfoEntityTmp.setHardwareHash(calcHardwareInfoHash);
            hardwareInfoDAO.insert(hardwareInfoEntityTmp);
        }
        return hardwareInfoEntityTmp;
    }

}
