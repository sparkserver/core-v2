/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.bo.util;

import java.util.List;

public class ServerInformationVO {
    private String messageSrv;

    private String homePageUrl;
    private String facebookUrl;
    private String discordUrl;

    private String serverName;
    private String country;
    private Integer timezone;
    private String bannerUrl;
    private String adminList;
    private String ownerList;
    private Long numberOfRegistered;
    private Integer secondsToShutDown = 7200;
    private String allowedCountries;
    private String webSignupUrl;
    private String passwordResetUrl;
    private String iconUrl;

    private List<String> activatedHolidaySceneryGroups;
    private List<String> disactivatedHolidaySceneryGroups;

    private Long onlineNumber;
    private boolean requireTicket = false;
    private String serverVersion;
    private boolean modernAuthSupport;

    public String getMessageSrv() {
        return messageSrv;
    }

    public void setMessageSrv(String messageSrv) {
        this.messageSrv = messageSrv;
    }

    public String getHomePageUrl() {
        return homePageUrl;
    }

    public void setHomePageUrl(String homePageUrl) {
        this.homePageUrl = homePageUrl;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getDiscordUrl() {
        return discordUrl;
    }

    public void setDiscordUrl(String discordUrl) {
        this.discordUrl = discordUrl;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getTimezone() {
        return timezone;
    }

    public void setTimezone(Integer timezone) {
        this.timezone = timezone;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public String getAdminList() {
        return adminList;
    }

    public void setAdminList(String adminList) {
        this.adminList = adminList;
    }

    public String getOwnerList() {
        return ownerList;
    }

    public void setOwnerList(String ownerList) {
        this.ownerList = ownerList;
    }

    public Long getNumberOfRegistered() {
        return numberOfRegistered;
    }

    public void setNumberOfRegistered(Long numberOfRegistered) {
        this.numberOfRegistered = numberOfRegistered;
    }

    public Integer getSecondsToShutDown() {
        return secondsToShutDown;
    }

    public void setSecondsToShutDown(Integer secondsToShutDown) {
        this.secondsToShutDown = secondsToShutDown;
    }

    public String getAllowedCountries() {
        return allowedCountries;
    }

    public void setAllowedCountries(String allowedCountries) {
        this.allowedCountries = allowedCountries;
    }

    public List<String> getActivatedHolidaySceneryGroups() {
        return activatedHolidaySceneryGroups;
    }

    public void setActivatedHolidaySceneryGroups(List<String> activatedHolidaySceneryGroups) {
        this.activatedHolidaySceneryGroups = activatedHolidaySceneryGroups;
    }

    public List<String> getDisactivatedHolidaySceneryGroups() {
        return disactivatedHolidaySceneryGroups;
    }

    public void setDisactivatedHolidaySceneryGroups(List<String> disactivatedHolidaySceneryGroups) {
        this.disactivatedHolidaySceneryGroups = disactivatedHolidaySceneryGroups;
    }

    public Long getOnlineNumber() {
        return onlineNumber;
    }

    public void setOnlineNumber(Long onlineNumber) {
        this.onlineNumber = onlineNumber;
    }

    public boolean isRequireTicket() {
        return requireTicket;
    }

    public void setRequireTicket(boolean requireTicket) {
        this.requireTicket = requireTicket;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public boolean isModernAuthSupport() {
        return modernAuthSupport;
    }

    public void setModernAuthSupport(boolean modernAuthSupport) {
        this.modernAuthSupport = modernAuthSupport;
    }

    public String getWebSignupUrl() {
        return webSignupUrl;
    }

    public void setWebSignupUrl(String webSignupUrl) {
        this.webSignupUrl = webSignupUrl;
    }

    public String getPasswordResetUrl() {
        return passwordResetUrl;
    }

    public void setPasswordResetUrl(String passwordResetUrl) {
        this.passwordResetUrl = passwordResetUrl;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
