/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.bo.BasketBO;
import com.soapboxrace.core.bo.ParameterBO;
import com.soapboxrace.core.dao.PersonaDAO;
import com.soapboxrace.core.dao.ProductDAO;
import com.soapboxrace.core.jpa.PersonaEntity;
import com.soapboxrace.core.jpa.ProductEntity;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/Admin")
public class AdminAPI {
	
	@EJB
	private ParameterBO parameterBO;

	@EJB
	private PersonaDAO personaDAO;

	@EJB
    private ProductDAO productDAO;

	@EJB
	private BasketBO basketBO;

	@POST
	@Path("/addCar")
	public Response addCar(@HeaderParam("Authorization") String apiKey,
					   @FormParam("productId") String productId,
					   @FormParam("personaId") long personaId) {
		String correctApiKey = parameterBO.getStrParam("ADMIN_AUTH");
		if (correctApiKey == null || !correctApiKey.equals(apiKey)) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}

		PersonaEntity personaEntity = personaDAO.findById(personaId);
		if (personaEntity == null) {
			return Response.status(Response.Status.BAD_REQUEST)
				.entity("no such persona")
				.type(MediaType.TEXT_PLAIN)
				.build();
		}

        ProductEntity productEntity;
		try {
		    productEntity = productDAO.findByProductId(productId);
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("no such product")
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }

		basketBO.addCar(productEntity, personaEntity);
		return Response.noContent().build();
	}
}
