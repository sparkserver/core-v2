/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.vo.notifications;

import com.soapboxrace.core.jpa.BanEntity;
import com.soapboxrace.core.jpa.PersonaEntity;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class BanNotification {
    private PersonaInfo persona;
    private PersonaInfo bannedBy;

    private String reason;
    private String endsAt;

    public BanNotification(PersonaEntity persona, BanEntity ban) {
        this.persona = new PersonaInfo(persona);
        this.bannedBy = new PersonaInfo(ban.getBannedBy());
        this.reason = ban.getReason();
        this.endsAt = DateTimeFormatter.ISO_INSTANT.format(ban.getEndsAt().atZone(ZoneId.systemDefault()).toInstant());
    }
}
